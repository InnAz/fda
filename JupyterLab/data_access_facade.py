import data_access
from datetime import date
from typing import Tuple
import pandas as pd
import data_access

class DataAccessFacade:
    @staticmethod
    def get_agvs() -> pd.DataFrame:
        # Rückgabewert: pandas DataFrame mit allen AGVs [agv]
        return data_access.get_agvs()

    @staticmethod
    def get_amount_finished_jobs(start_time, end_time) -> pd.DataFrame:
        # Parameter:
        # - start_time (date): Startzeitpunkt der Abfrage
        # - end_time (date): Endzeitpunkt der Abfrage
        # Rückgabewert: pandas DataFrame [agv, count_finished_jobs]
        return data_access.get_amount_finished_jobs(start_time, end_time)

    @staticmethod
    def get_positions(agv, start_time, end_time, filter_null_jobid = False) -> pd.DataFrame:
         # Parameter:
         # - agv (str): ID des AGVs
         # - start_time (date): Startzeitpunkt der Abfrage
         # - end_time (date): Endzeitpunkt der Abfrage
         # - filter_null_jobid (bool): Optional, boolescher Wert, ob Positionen ohne Job-ID gefiltert werden sollen (Standard: False)
         # Rückgabewert: pandas DataFrame [pose_x, pose_y, timestamp, jobid]
        return data_access.get_positions(agv, start_time, end_time, filter_null_jobid)

    @staticmethod
    def get_job_start_endtimes(agv, start_time, end_time) -> pd.DataFrame:
        # Parameter:
        # - agv (str): ID des AGVs
        # - start_time (date): Startzeitpunkt der Abfrage
        # - end_time (date): Endzeitpunkt der Abfrage
        # Rückgabewert: pandas DataFrame [starttime, endtime]
        return data_access.get_job_start_endtimes(agv, start_time, end_time)

    @staticmethod
    def get_interruption_data(start_time, end_time) -> pd.DataFrame:
         # Parameter:
         # - start_time (date): Startzeitpunkt der Abfrage
         # - end_time (date): Endzeitpunkt der Abfrage
         # Rückgabewert: pandas DataFrame [agv, total_loss_time]
        return data_access.get_interruption_data(start_time, end_time)

    @staticmethod
    def count_top_jobgoals(start_time, end_time) -> pd.DataFrame:
         # Parameter:
         # - start_time (date): Startzeitpunkt der Abfrage
         # - end_time (date): Endzeitpunkt der Abfrage
         # Rückgabewert: pandas DataFrame [jobgoal, frequency]
        return data_access.count_top_jobgoals(start_time, end_time)

    @staticmethod
    def get_goal_positions(jobgoal) -> pd.DataFrame:
         # Parameter:
         # - jobgoal (str): ID des Jobziels
         # Rückgabewert: pandas DataFrame [jobgoal, pose_x, pose_y]
        return data_access.get_goal_positions(jobgoal)

    @staticmethod
    def get_pos_source_and_target(selected_job) -> Tuple[pd.DataFrame, pd.DataFrame]:
        # Parameter:
         # - selected_job (str): ID des ausgewählten Jobs
         # Rückgabewert: Tuple[pandas DataFrame [pose_x, pose_y, sourcegoalid], pandas DataFrame [pose_x, pose_y, targetgoalid]]
        return data_access.get_pos_source_and_target(selected_job)

    @staticmethod
    def get_jobgoals_with_pos(start_time, end_time) -> pd.DataFrame:
       # Parameter:
         # - start_time (date): Startzeitpunkt der Abfrage
         # - end_time (date): Endzeitpunkt der Abfrage
         # Rückgabewert: pandas DataFrame [pose_x, pose_y, timestamp, id, jobid]
        return data_access.get_jobgoals_with_pos(start_time, end_time)

    @staticmethod
    def get_positions_all(start_time, end_time) -> pd.DataFrame:
        # Parameter:
         # - start_time (date): Startzeitpunkt der Abfrage
         # - end_time (date): Endzeitpunkt der Abfrage
         # Rückgabewert: pandas DataFrame [pose_x, pose_y, agv, jobid, timestamp]
        return data_access.get_positions_all(start_time, end_time)






