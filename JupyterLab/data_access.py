import pandas as pd
from db_conn import engine
from datetime import datetime, time, date
import pytz


def get_agvs():
    return pd.read_sql("SELECT id FROM azarova_bachelor.agv", engine)

def get_positions(agv, start_time, end_time, filter_null_jobid=False):
    query_positions = f"""
        SELECT pose_x, pose_y, timestamp, jobid
        FROM azarova_bachelor.agvposition 
        WHERE agvid = '{agv}' AND timestamp BETWEEN '{start_time}' AND '{end_time}' 
    """
    if filter_null_jobid:
        query_positions += " AND jobid IS NOT NULL"
    query_positions += " ORDER BY timestamp"
    return pd.read_sql(query_positions, engine)

    
def get_amount_finished_jobs(start_time, end_time):
    # SQL-Abfrage zur Extrahierung der abgeschlossenen Jobs pro AGV für den angegebenen Zeitraum
    query_count_f_jobs = f"""
        SELECT job.agvid, COUNT(job.id) AS job_count
        FROM azarova_bachelor.job
        INNER JOIN azarova_bachelor.jobstatus ON job.id = jobstatus.jobid
        WHERE jobstatus.status = 'Finished'
        AND job.starttime BETWEEN '{start_time}' AND '{end_time}'
        GROUP BY job.agvid
    """
    return pd.read_sql(query_count_f_jobs, engine)

def get_job_start_endtimes(agv, start_time, end_time):
    # Wenn start_time ein date Objekt ist
    if type(start_time) is date and not type(start_time) is datetime:
        # Wandlung eines datetime Objekts in eine Zeit (Anfang des Tages) 
        start_time = datetime.combine(start_time, time(0, 0, 0))
        # datetime mit Zeitzone (sonst Fehler)
        start_time = pytz.utc.localize(start_time)

    # Wenn end_time ein date Objekt ist
    if type(end_time) is date and not type(end_time) is datetime:
         # Wandlung eines datetime Objekts in eine Zeit (Ende des Tages) 
        end_time = datetime.combine(end_time, time(23, 59, 59))
        # datetime mit Zeitzone (sonst Fehler)
        end_time = pytz.utc.localize(end_time)
        
    query_job_times = f"""
        SELECT starttime, endtime
        FROM azarova_bachelor.job
        WHERE agvid = '{agv}' AND starttime >= '{start_time}' AND endtime <= '{end_time}'
    """
    return pd.read_sql(query_job_times, engine)

def get_interruption_data(start_time, end_time):
    query_interruption_time = f"""
        SELECT j.agvid, FLOOR(SUM(EXTRACT(EPOCH FROM (ji.endtime - ji.starttime))) / 60.0) as total_loss_time
        FROM azarova_bachelor.jobinterruption ji
        INNER JOIN azarova_bachelor.job j
        ON ji.jobid = j.id
        WHERE ji.starttime BETWEEN '{start_time}' AND '{end_time}' 
        AND ji.endtime BETWEEN '{start_time}' AND '{end_time}'
        GROUP BY j.agvid
    """
    return pd.read_sql(query_interruption_time, engine)

def count_top_jobgoals(start_time, end_time):
    query_top_goals = f"""
        SELECT jobgoal.id, COUNT(*) AS frequency
        FROM azarova_bachelor.jobgoal
        WHERE jobgoal.timestamp BETWEEN '{start_time}' AND '{end_time}'
        GROUP BY jobgoal.id
        ORDER BY frequency DESC
        LIMIT 20
    """
    return pd.read_sql(query_top_goals, engine)

def get_goal_positions(jobgoal):
    query_goal_pos = f"""SELECT goal.id, goal.pose_x, goal.pose_y 
        FROM azarova_bachelor.goal WHERE goal.id = '{jobgoal}'"""     
    result = pd.read_sql(query_goal_pos, engine) 
    # Benennt die Spalte 'goal.id' in 'jobgoal' um
    result = result.rename(columns={'goal.id': 'jobgoal'})
    return result

def get_pos_source_and_target(selected_job):
    # SQL-Abfrage zur Extrahierung der Source- und Target-Goals für den ausgewählten Job
    sql_command_sourcegoal = f"""
        SELECT goal.pose_x, goal.pose_y, goal.id
        FROM azarova_bachelor.job
        INNER JOIN azarova_bachelor.goal ON job.sourcegoalid = goal.id
        WHERE job.id = '{selected_job}'
    """
    sql_command_targetgoal = f"""
        SELECT goal.pose_x, goal.pose_y, goal.id
        FROM azarova_bachelor.job
        INNER JOIN azarova_bachelor.goal ON job.targetgoalid = goal.id
        WHERE job.id = '{selected_job}'
    """
    df_sourcegoal = pd.read_sql(sql_command_sourcegoal, engine)
    df_targetgoal = pd.read_sql(sql_command_targetgoal, engine)
    return df_sourcegoal,df_targetgoal

def get_jobgoals_with_pos(start_time, end_time):
    # SQL-Abfrage zur Extrahierung der Jobgoals
    sql_command_jobgoals =f"""
         SELECT goal.pose_x, goal.pose_y,jobgoal.timestamp, jobgoal.id, jobgoal.jobid
         FROM azarova_bachelor.jobgoal
         INNER JOIN azarova_bachelor.goal ON jobgoal.id = goal.id
         WHERE timestamp BETWEEN '{start_time}' AND '{end_time}'
    """
    return pd.read_sql(sql_command_jobgoals, engine)

def get_positions_all(start_time, end_time):
    sql_command_positions = f"""
        SELECT pose_x, pose_y, agvid, jobid, timestamp
        FROM azarova_bachelor.agvposition
        WHERE timestamp BETWEEN '{start_time}' AND '{end_time}'
    """ 
    return pd.read_sql(sql_command_positions, engine)
