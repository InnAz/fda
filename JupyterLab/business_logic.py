import numpy as np
import pandas as pd
from data_access_facade import DataAccessFacade
from datetime import datetime, time, date
import pytz

def determine_throughput(start_time, end_time):
    return DataAccessFacade.get_amount_finished_jobs(start_time, end_time)

def calculate_avg_job_time(start_time, end_time):
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agv', 'avg_job_time'])
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv = row['id']
        jobs_df = DataAccessFacade.get_job_start_endtimes(agv, start_time, end_time)
        jobs_df['job_time'] = (jobs_df['endtime'] - jobs_df['starttime']).dt.total_seconds() / 60
        avg_job_time = jobs_df['job_time'].mean()

        # Ergebnisse zum DataFrame hinzufügen
        results_df = pd.concat([results_df, pd.DataFrame({"agv": [agv], "avg_job_time": [avg_job_time]})], ignore_index=True)
    return results_df.sort_values('agv')

def calculate_job_time(start_time, end_time):
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agv', 'job_time'])
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv = row['id']
        jobs_df = DataAccessFacade.get_job_start_endtimes(agv, start_time, end_time)
        # Berechnung der absoluten Jobzeit
        jobs_df['job_time'] = (jobs_df['endtime'] - jobs_df['starttime']).dt.total_seconds() / 60 / 60
        job_time = jobs_df['job_time'].sum()
        # Ergebnisse zum DataFrame hinzufügen
        results_df = pd.concat([results_df, pd.DataFrame({"agv": [agv], "job_time": [job_time]})], ignore_index=True)
    return results_df.sort_values('agv')

def calculate_utilization(start_time, end_time):
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agv', 'utilization'])
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv_id = row['id']
        jobs_df = DataAccessFacade.get_job_start_endtimes(agv_id, start_time, end_time)
        
        # Berechnung der Gesamtauslastung
        # Synchronisieren der Zeitzoneninformation und Konvertieren in datetime, falls notwendig
        if isinstance(start_time, date) and not isinstance(start_time, datetime):
            start_time = datetime.combine(start_time, time(0, 0, 0), tzinfo=end_time.tzinfo if isinstance(end_time, datetime) else None)
        if isinstance(end_time, date) and not isinstance(end_time, datetime):
            end_time = datetime.combine(end_time, time(23, 59, 59), tzinfo=start_time.tzinfo if isinstance(start_time, datetime) else None)
        
        total_time = (end_time - start_time).total_seconds() / 60
        utilized_time = (jobs_df['endtime'] - jobs_df['starttime']).dt.total_seconds().sum() / 60
        utilization = utilized_time / total_time * 100
        
        # Temporäres DataFrame erstellen
        temp_df = pd.DataFrame({"agv": [agv_id], "utilization": [utilization]})
        
        # Ergebnisse zum DataFrame hinzufügen
        results_df = pd.concat([results_df, temp_df], ignore_index=True)
    
    return results_df.sort_values('agv')

def calculate_avg_speed(start_time, end_time):
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agv', 'avg_speed']) 
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv = row['id']
        positions_df = DataAccessFacade.get_positions(agv, start_time, end_time, True)
        # Berechnung der Geschwindigkeit für alle Positionen
        positions_df['diff_x'] = positions_df['pose_x'].diff()
        positions_df['diff_y'] = positions_df['pose_y'].diff()
        positions_df['distance'] = np.sqrt(positions_df['diff_x']**2 + positions_df['diff_y']**2)
        positions_df['time_diff'] = positions_df['timestamp'].diff().dt.total_seconds()
        positions_df['speed'] = positions_df['distance'] / positions_df['time_diff']

        # Entfernen der Zeilen, in denen die Geschwindigkeit gleich 0 ist
        positions_df = positions_df[positions_df['speed'] != 0] 
        # Berechnung der Durchschnittsgeschwindigkeit - (auch 0 Geschwindigkeiten in df enthalten)
        avg_speed = positions_df['speed'].mean()
        # Ergebnisse zum DataFrame hinzufügen
        results_df = pd.concat([results_df, pd.DataFrame({"agv": [agv], "avg_speed": [avg_speed]})], ignore_index=True)
    return results_df.sort_values('agv')

def calculate_stop_time(start_time, end_time):
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agv', 'stoptime'])
    
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv = row['id']
        positions_df = DataAccessFacade.get_positions(agv, start_time, end_time, True)
        # Berechnung der Geschwindigkeit
        positions_df['diff_x'] = positions_df['pose_x'].diff()
        positions_df['diff_y'] = positions_df['pose_y'].diff()
        positions_df['distance'] = np.sqrt(positions_df['diff_x']**2 + positions_df['diff_y']**2)
        positions_df['time_diff'] = positions_df['timestamp'].diff().dt.total_seconds()
        positions_df['speed'] = positions_df['distance'] / positions_df['time_diff']

        # Berechnung der Standzeit
        positions_df['stoptime'] = (positions_df['speed'] == 0).astype(int)
        total_jobs = len(positions_df['jobid'].unique())
        total_stoptime = positions_df['stoptime'].sum() / 60 / total_jobs
        # Ergebnisse zum DataFrame hinzufügen
        results_df = pd.concat([results_df, pd.DataFrame({"agv": [agv], "stoptime": [total_stoptime]})], ignore_index=True)

    return results_df.sort_values('agv')

def calculate_interruption_time(start_time, end_time):
    start_time_str = start_time.strftime("%Y-%m-%d %H:%M:%S")
    end_time_str = end_time.strftime("%Y-%m-%d %H:%M:%S")
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agvid', 'total_lost_time'])
    results_df = DataAccessFacade.get_interruption_data(start_time_str, end_time_str, )
    return results_df.sort_values('agvid')

def calculate_avg_interruption_time(start_time, end_time):
    start_time_str = start_time.strftime("%Y-%m-%d %H:%M:%S")
    end_time_str = end_time.strftime("%Y-%m-%d %H:%M:%S")
    # Erstellung eines leeren DataFrames für die Ergebnisse
    results_df = pd.DataFrame(columns=['agvid', 'total_lost_time'])
    results_df = DataAccessFacade.get_interruption_data(start_time_str, end_time_str)

    # Get finished jobs for each AGV
    jobs_df = DataAccessFacade.get_amount_finished_jobs(start_time, end_time)
    jobs_df.set_index('agvid', inplace=True)

    # Calculate the average lost time per job
    results_df['avg_loss_time_per_job'] = results_df.apply(lambda row: row['total_loss_time'] / jobs_df.loc[row['agvid'], 'job_count'], axis=1)
    return results_df.sort_values('agvid')

def calculate_distance(start_time, end_time):
    results_df = pd.DataFrame(columns=["agv", "total_distance"])
    agv = DataAccessFacade.get_agvs()
    for index, row in agv.iterrows():
        agv = row['id']
        df = DataAccessFacade.get_positions(agv, start_time, end_time)
        df['diff_x'] = df['pose_x'].diff()
        df['diff_y'] = df['pose_y'].diff()
        df['dist'] = np.sqrt(df['diff_x']**2 + df['diff_y']**2)
        total_distance = df['dist'].sum() / 1000
        results_df = pd.concat([results_df, pd.DataFrame({"agv": [agv], "total_distance": [total_distance]})], ignore_index=True)
        results_df = results_df.sort_values('agv')
    # DataFrame nach 'AGV' sortieren
    return results_df
    
def determine_top_jobgoals(start_time, end_time):  
    df_top_jobgoals = DataAccessFacade.count_top_jobgoals(start_time, end_time)
    df_jobgoals_positions = pd.DataFrame(columns=["jobgoal", "pose_x", "pose_y"])
    for index, row in df_top_jobgoals.iterrows():
        result_df = DataAccessFacade.get_goal_positions(row['id'])
        # fügt das Ergebnis-DataFrame zum Gesamtergebnis-DataFrame hinzu
        df_jobgoals_positions = pd.concat([df_jobgoals_positions, result_df], ignore_index=True)

    return df_top_jobgoals, df_jobgoals_positions

def determine_jobgoals(start_time, end_time):
    return DataAccessFacade.get_jobgoals_with_pos(start_time, end_time)

def determine_positions(start_time, end_time):
    return DataAccessFacade.get_positions_all(start_time, end_time)
def determine_source_target(jobid):
    return DataAccessFacade.get_pos_source_and_target(jobid)







    
