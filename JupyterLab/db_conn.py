import psycopg2
from sqlalchemy import create_engine
import configparser

# Konfigurationsdatei lesen
config = configparser.ConfigParser()
config.read('config.ini')
#config.read(r'C:\Users\Inna Azarova\Desktop\bachelorAzarova\FDA\JupyterLab\config.ini')

# Werte aus der Konfigurationsdatei holen
user = config.get('database', 'user')
password = config.get('database', 'password')
host = config.get('database', 'host')
dbname = config.get('database', 'dbname')

# Verbindung zur PostgreSQL-Datenbank herstellen
engine = create_engine(f'postgresql+psycopg2://{user}:{password}@{host}:5432/{dbname}')