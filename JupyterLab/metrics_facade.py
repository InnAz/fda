import business_logic
from datetime import datetime, time, date
import pandas as pd

class MetricsFacade:

    @staticmethod
    def calculate_avg_speed(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die durchschnittliche Geschwindigkeit für jedes AGV 
        # innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur durchschnittlichen 
        # Geschwindigkeit für jedes AGV, mit den Spalten 'agv' und 'avg_speed'.
        return business_logic.calculate_avg_speed(start_time, end_time)
    
    @staticmethod
    def determine_throughput(start_time, end_time):# -> pd.DataFrame:
        # Berechnet den Durchsatz der Jobs innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Durchsatzdaten für jedes AGV, mit den Spalten 'agv' und 'count_finished_jobs'.
        return business_logic.determine_throughput(start_time, end_time)

    @staticmethod
    def calculate_avg_job_time(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die durchschnittliche Jobdauer für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur durchschnittlichen Jobdauer für jedes AGV, mit den Spalten 'agv' und 'avg_job_time'.
        return business_logic.calculate_avg_job_time(start_time, end_time)

    @staticmethod
    def calculate_job_time(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die Gesamtdauer der Jobs für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur Gesamtdauer der Jobs für jedes AGV, mit den Spalten 'agv' und 'job_time'.
        return business_logic.calculate_job_time(start_time, end_time)

    @staticmethod
    def calculate_utilization(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die Auslastung für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (datetime): Der Startzeitpunkt des Zeitraums.
        # - end_time (datetime): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur Auslastung für jedes AGV, mit den Spalten 'agv' und 'utilization'.
        return business_logic.calculate_utilization(start_time, end_time)

    @staticmethod
    def calculate_stop_time(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die durchschnittliche Stillstandsdauer pro Job für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur durchschnittlichen Stillstandsdauer für jedes AGV, mit den Spalten 'agv' und 'stoptime'.
        return business_logic.calculate_stop_time(start_time, end_time)

    @staticmethod
    def calculate_interruption_time(start_time, end_time):# -> pd.DataFrame:
        # Berechnet die Gesamtunterbrechungsdauer für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeitpunkt des Zeitraums.
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur Gesamtunterbrechungsdauer für jedes AGV, mit den Spalten 'agvid' und 'total_loss_time'.
        return business_logic.calculate_interruption_time(start_time, end_time)

    @staticmethod
    def calculate_avg_interruption_time(start_time, end_time): #-> pd.DataFrame:
        # Berechnet die durchschnittliche Unterbrechungsdauer pro Job für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeit
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur durchschnittlichen Unterbrechungsdauer für jedes AGV, mit den Spalten 'agvid' und 'avg_loss_time'.
        return business_logic.calculate_interruption_time(start_time, end_time)

    @staticmethod
    def calculate_distance(start_time, end_time): # -> pd.DataFrame
        # Berechnet den zurückgelegeten Weg für jedes AGV innerhalb des angegebenen Zeitraums.
        # Parameter:
        # - start_time (date): Der Startzeit
        # - end_time (date): Der Endzeitpunkt des Zeitraums.
        # Rückgabewert: pandas DataFrame mit den Daten zur zurückgelegeten Weg für jedes AGV, mit den Spalten 'agvid' und 'distance'.
        return business_logic.calculate_distance(start_time, end_time)

    @staticmethod
    def determine_top_jobgoals(start_time, end_time): #-> pd.DataFrame,pd.DataFrame
         # ermittelt Daten von 20 Top jobgoals mit ihren Koordinaten und Häufigkeit innerhalb des angegebenen Zeitraums.
         # Parameter:
         # - start_time (date): Der Startzeit
         # - end_time (date): Der Endzeitpunkt des Zeitraums.
         # Rückgabewert: zwei pandas DataFrame mit den Daten von 20 Top jobgoals mit ihren Koordinaten und Häufigkeit ["jobgoal", 'frequency'] ["jobgoal","pose_x", "pose_y"]
        return business_logic.determine_top_jobgoals(start_time, end_time)

    @staticmethod
    def determine_jobgoals(start_time, end_time):#-> pd.DataFrame
         # gibt alle jobgoals mit Koordinaten für angegebenen Zeitraums aus
         # Parameter:
         # - start_time (date): Der Startzeit
         # - end_time (date): Der Endzeitpunkt des Zeitraums.
         # Rückgabewert: pandas DataFrame mit den Daten für Jobgoals 'pose_x', 'pose_y','timestamp', 'id','jobid'
        return business_logic.determine_jobgoals(start_time, end_time)

    @staticmethod
    def determine_positions(start_time, end_time):# -> pd.DataFrame
         # gibt alle Werte für Positionsdaten innerhalb des angegebenen Zeitraums aus.
         # Parameter:
         # - start_time (date): Der Startzeit
         # - end_time (date): Der Endzeitpunkt des Zeitraums.
         # Rückgabewert: pandas DataFrame mit den Daten für Positionen 'pose_x', 'pose_y', 'agvid', 'jobid', 'timestamp'
        return business_logic.determine_positions(start_time, end_time)

    @staticmethod
    def determine_source_target(jobid):# -> pd.DataFrame,pd.DataFrame
         # ermittelt Koordinaten für source und target für einen Job
         # Parameter:
         # - jobid (String): Job-ID
         # Rückgabewert: zwei pandas DataFrame mit den Daten 'pose_x', 'pose_y' 'goilid' .
        return business_logic.determine_source_target(jobid)
    