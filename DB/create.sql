DROP SCHEMA azarova_bachelor CASCADE;

CREATE SCHEMA azarova_bachelor AUTHORIZATION azarova;


-- Drop table

-- DROP TABLE azarova_bachelor.goal;

CREATE TABLE azarova_bachelor.goal (
	id varchar NOT NULL,
	pose_x float8 NOT NULL,
	pose_y float8 NOT NULL,
	CONSTRAINT goal_pk PRIMARY KEY (id)
);
-- Drop table

-- DROP TABLE azarova_bachelor.agv;

CREATE TABLE azarova_bachelor.agv (
	id varchar NOT NULL,
	"name" varchar NULL,
	CONSTRAINT agv_pk PRIMARY KEY (id)
);
-- Drop table

-- DROP TABLE azarova_bachelor.job;

CREATE TABLE azarova_bachelor.job (
	id varchar NOT NULL,
	agvid varchar NULL,
	starttime timestamp NOT NULL,
	endtime timestamp NULL,
	sourcegoalid varchar NOT NULL,
	targetgoalid varchar NOT NULL,
	CONSTRAINT jobexecute_pk PRIMARY KEY (id),
	CONSTRAINT jobexecute_fk_1 FOREIGN KEY (agvid) REFERENCES azarova_bachelor.agv(id),
	CONSTRAINT jobexecution_fk FOREIGN KEY (sourcegoalid) REFERENCES azarova_bachelor.goal(id),
	CONSTRAINT jobexecution_fk_1 FOREIGN KEY (targetgoalid) REFERENCES azarova_bachelor.goal(id)
);

-- Drop table

-- DROP TABLE azarova_bachelor.agvposition;

CREATE TABLE azarova_bachelor.agvposition (
	pose_x float8 NOT NULL,
	pose_y float8 NOT NULL,
	pose_o float8 NOT NULL,
	agvid varchar NOT NULL,
	"timestamp" timestamp NOT NULL,
	jobid varchar NULL,
	CONSTRAINT agvposition_pk PRIMARY KEY ("timestamp", agvid),
	CONSTRAINT agv_id_fk FOREIGN KEY (agvid) REFERENCES azarova_bachelor.agv(id),
	CONSTRAINT agvposition_fk FOREIGN KEY (jobid) REFERENCES azarova_bachelor.job(id)
);




-- Drop table

-- DROP TABLE azarova_bachelor.jobgoal;

CREATE TABLE azarova_bachelor.jobgoal (
	id varchar NOT NULL,
	jobid varchar NULL,
	"timestamp" timestamp NOT NULL,
	CONSTRAINT jobgoal_pk PRIMARY KEY (id, "timestamp"),
	CONSTRAINT jobgoal_fk FOREIGN KEY (jobid) REFERENCES azarova_bachelor.job(id),
	CONSTRAINT jobgoal_fk_1 FOREIGN KEY (id) REFERENCES azarova_bachelor.goal(id)
);

-- Drop table

-- DROP TABLE azarova_bachelor.jobinterruption;

CREATE TABLE azarova_bachelor.jobinterruption (
	id varchar NOT NULL,
	jobid varchar NOT NULL,
	starttime timestamp NOT NULL,
	endtime timestamp NULL,
	description varchar NOT NULL,
	CONSTRAINT jobinterruption_pk PRIMARY KEY (id),
	CONSTRAINT jobinterruption_fk FOREIGN KEY (jobid) REFERENCES azarova_bachelor.job(id)
);

-- Drop table

-- DROP TABLE azarova_bachelor.jobstatus;

CREATE TABLE azarova_bachelor.jobstatus (
	jobid varchar NOT NULL,
	status varchar NOT NULL,
	"timestamp" timestamp NOT NULL,
	CONSTRAINT jobstatus_pk PRIMARY KEY (jobid, status, "timestamp"),
	CONSTRAINT jobexe_fk FOREIGN KEY (jobid) REFERENCES azarova_bachelor.job(id)
);
