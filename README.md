
# FTS-Datenanalyse-Anwendung (FTA)

FTA besteht aus 3 Komponenenten: eine PostgreSQL DB,eine Parser-Anwendung und eine Datenanalyse-Anwendung basierend auf Jupyterlab

## Installation und Anforderungen
 
Python,Postgresql und DBeaver können von den offiziellen Websiten heruntergeladen werden 
### **Python installieren:** 
(https://www.python.org) 

### **PostgreSQL installieren:** 
(https://www.postgresql.org) 

### **DBeaver installieren:** 
DBeaver ist ein beliebtes Datenbankverwaltungswerkzeug, das optional verwendet werden kann, um die PostgreSQL-Datenbank zu verwalten und Abfragen auszuführen.  
(https://dbeaver.io) 

## *Datenbank*
  ## SQL-Tabellen anlegen um die Parser-Anwendung zu testen
  create.sql
  ## Tabellen inklusiver Daten importieren um die Datenanalyse-Anwendung zu testen

  Für den Import kann DBeaver oder ein Konsolenbefehl benutzt werden
   ```
   pg_restore -U <username> -d <databasename> -v <filename.dump>
   
   ```
  
  dump-Dateien in folgeneder Reihenfolge importieren:
  dump-azarova_bachelor-202307071152
  dump-azarova_bachelor-202307092158_agvposition


## *Parser-Anwendung*

### **Abhängigkeiten installieren:**
   psycopg2 ermöglicht die Kommunikation zwischen Python-Anwendungen und einer PostgreSQL-Datenbank.
   psycopg2 wird mit dem folgenden Befehl installiert:
   ```
   pip install psycopg2
   
   ```

### db_conn_config.ini
   
1. Parameter für die DB Verbindung müssen in der Datei angepasst werden.  
```
        [database] Schlüsselwort und darf nicht verwendet werden
        user = z.B.azarova
        password = ******
        host = z.B.localhost
        dbname = z.B.azarova_bachelor
        schema = z.B.azarova_bachelor
     
```
### path_config.ini
2. Die Log- und Goals-Dateien sollen lokal zugänglich abgespeichert werden. Entsprechende Verzeichnisse müssen in der Datei eingetragen werden.
```
        [paths]
        log_directory = C:\Users\Inna Azarova\Desktop\bachelorAzarova\bachelor\logs_in
        out_directory = C:\Users\Inna Azarova\Desktop\bachelorAzarova\bachelor\logs_out
        csv_directory = C:\Users\Inna Azarova\Desktop\bachelorAzarova\bachelor\goals
```
## parse_control.py
3. Wenn die INI-Datei nicht erkannt wird, muss config.read('path_config.ini') auskommentiert werden und der Pfad der INI-Datei eingetragen werden: config.read(r'your Path to ini \path_config.ini')

## db_conn.py
4. Wenn die INI-Datei nicht erkannt wird, muss config.read('db_conn_config.ini') auskommentiert werden und der Pfad der INI-Datei eingetragen werden: config.read(r'your Path to ini \db_conn_config.ini')
5. Parser-Anwendung ausführen
```
   python parse_control.py
   
```

## *Datenanalyse-Anwendung*

### **Eine virtuelle Umgebung zu erstellen:** 

   ```
   python3 -m venv venv
   ```
  #### Aktivierung 
   ```
   source venv/bin/activate (unter macOS/Linux)

   .\venv\Scripts\activate (unter Windows)
   ```

### **Datenanalyse mit Jupyter Lab:** 
   JupyterLab wird mit dem folgenden Befehl installiert:
   ```
   pip install jupyterlab
   ```

   Jupyter Lab starten
   ```
   jupyter lab

   ```
  Nachdem sich JupyterLab im Browser geöffnet hat, navigieren Sie im linken Datenexplorer zu der bereitgestellten Datei "Datenanalyse_JLab.ipynb" und wählen Sie sie aus. 

### **Abhängigkeiten installieren:**
   Installation der erforderlichen Bibliotheken sind in der Datenanalyse_JLab.ipnb vordefiniert.
   Die jeweiligen Zellen müssen ausgeführt werden, damit die JupyterLab-Anwendung funktioniert
  

## config.ini
   
1. Parameter für die DB Verbindung müssen in der Datei angepasst werden.  
```
        [database] Schlüsselwort und darf nicht verwendet werden
        user = z.B.azarova
        password = ******
        host = z.B.localhost
        dbname = z.B.azarova_bachelor
     
```
## Anwendung ausführen
   Die Datei "Datenanalyse_JLab.ipynb" enthält den vorgefertigten Code und die Anweisungen für die Durchführung der Analyse. 
   Für die Darstellung der Diagramme muss die jeweilige Zelle ausgeführt werden. Anschliessend kann ein Zeitinterval vorgegeben werden und über Button wird die Analyse ausgeführt.
 


