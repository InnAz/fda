import os
import csv
import shutil
import configparser
from db_access import save_in_db
from data_extraction import *
from db_access import save_in_db, save_goals_in_db
from db_conn import conn


def parse_data(csv_directory,log_directory,out_directory):
    insert_agv_data = set()
    insert_job_data = []
    job_endtimes = {}
    insert_jobstatus_data = []
    insert_jobinterruption_data = []
    jobinterruption_endtimes = {}
    insert_jobgoal_data = []
    insert_positions_data = []
    filenumber = 0

    try:
        csv_file_path = os.path.join(csv_directory, 'goals.csv')
        insert_goal_data = []

        with open(csv_file_path, newline='') as csvfile:
            csvreader = csv.DictReader(csvfile, delimiter=',')
            for row in csvreader:
                # extract goal data
                goals = extract_goal_data(row)
                if goals:
                    insert_goal_data.append(goals)
            save_goals_in_db(insert_goal_data)
                
        # Liste mit Logfiles erstellen
        log_files = [f for f in os.listdir(log_directory) if f.endswith(".log")]
        # Sortieren der Dateien basierend auf dem Änderungsdatum
        sorted_files = sorted(log_files, key=lambda x: os.path.getmtime(os.path.join(log_directory, x)))
        for filename in sorted_files:  # Schleife über alle Logfiles
            filenumber += 1
            with open(os.path.join(log_directory, filename), "r") as file:
                for line in file:  # Schleife über alle Zeilen
                    #extract agvs
                    agvs = extract_agvs(line)
                    if agvs:
                        insert_agv_data.add(agvs)

                    # extract job ----------
                    job_data = extract_job_data(line)
                    if job_data:
                        insert_job_data.append(job_data)

                    # extract job endtimes ----------
                    id, endtime = extract_job_endtimes(line)
                    if id:
                        job_endtimes[id] = endtime

                    # extract jobstatus -----------
                    jobstatus = extract_jobstatus(line)
                    if jobstatus:
                        insert_jobstatus_data.append(jobstatus)

                    # extract jobinterruption data -------------
                    jobinterruption = extract_joninterruptio(line) 
                    if jobinterruption[0]:
                        insert_jobinterruption_data.append(jobinterruption)                            
                                
                    #  extract jobinterruption endtime -----------
                    id, endtime = extract_jobinterr_endtimes(line)
                    if id:
                        jobinterruption_endtimes[id] = endtime
                    
                    #-exctract jobgoals -------------------
                    jobgoals = extract_jobgoals(line)
                    for jobgoal in jobgoals:
                        if jobgoal:
                            insert_jobgoal_data.append(jobgoal)
                    
                    #  extract AGV positions ---------------
                    agvpos = extract_agvposition(line)
                    if agvpos[0]:
                        insert_positions_data.append(agvpos)


                save_in_db(insert_agv_data, insert_job_data, job_endtimes,insert_jobstatus_data,insert_jobinterruption_data,jobinterruption_endtimes,insert_jobgoal_data,insert_positions_data)
            
            print(filenumber)
            #verschiebe abgearbeitete logfiles ins out-Verzeichnis
            shutil.move(os.path.join(log_directory, filename), os.path.join(out_directory, filename))

    finally:
        if conn:
            conn.close()
            print('Database connection closed.')    

# ----------------------- Main-----------------------------------------------------------
if __name__ == '__main__':  
    config = configparser.ConfigParser()
    #config.read('path_config.ini')
    # Wenn die INI-Datei nicht erkannt wird, dann die nachfolgende Zeile Auskommentierung entfernen und den Pfad der INI-Datei eintragen
    config.read(r'C:\****\path_config.ini')

    # Pfade aus der Konfigurationsdatei holen
    log_directory = config.get('paths', 'log_directory')
    out_directory = config.get('paths', 'out_directory')
    csv_directory = config.get('paths', 'csv_directory')
    # ------------ Aufruf der Parser-Funktionen ----------------------------------------
    parse_data(csv_directory,log_directory,out_directory)
    

