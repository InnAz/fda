from psycopg2.extras import execute_values
from db_conn import conn, SCHEMA


def save_goals_in_db(insert_goal_data):
    cur = None
    try: 
        cur=conn.cursor()
        #--------------- insert goal in db -----------------------------------
        # Füge die Daten in die Datenbank ein
        execute_values(cur, f"INSERT INTO {SCHEMA}.goal (id, pose_x, pose_y) VALUES %s ON CONFLICT (id) DO NOTHING",
                    insert_goal_data)
        conn.commit()  
    except Exception as e:
        print(f"Error during goal insertion: {e}")
        if cur:
            cur.close()
        if conn:
            conn.close()
            

def save_in_db(insert_agv_data, insert_job_data, job_endtimes,insert_jobstatus_data,insert_jobinterruption_data,jobinterruption_endtimes,insert_jobgoal_data,insert_positions_data):
    cur = None
    try: 
        cur=conn.cursor()
        # -------------- insert agv in db -------------------------------------
        execute_values(cur, """
                            INSERT INTO {}.agv (id, name) 
                            VALUES %s
                            ON CONFLICT (id) DO NOTHING;
                        """.format(SCHEMA), list(insert_agv_data))
        conn.commit()
        # -------------- insert job in db ---------------------------------------
        for job_info in insert_job_data:
            cur.execute("""
                            INSERT INTO {}.job (id, agvid, starttime, endtime, sourcegoalid, targetgoalid) 
                            VALUES (%s, %s, %s, %s, %s, %s)
                            ON CONFLICT (id)
                            DO UPDATE SET id = %s, agvid = %s, starttime = %s, endtime = %s, sourcegoalid = %s, targetgoalid = %s
                        """.format(SCHEMA), job_info * 2)  # Liste verdoppeln für den UPDATE-Teil der Anweisung
        conn.commit()
        # ----------- update endtimes in job Tabelle -------------------
        for job_id, endtime in job_endtimes.items():
            cur.execute("""
                            UPDATE {}.job
                            SET endtime = %s
                            WHERE id = %s
                        """.format(SCHEMA), (endtime, job_id))
        conn.commit()
        # -------------- insert jobinterruption in db -----------------------
        execute_values(cur, """
                            INSERT INTO {}.jobinterruption (id, jobid, starttime, description)
                            SELECT id, jobid, starttime, description
                            FROM (VALUES %s) AS subquery (id, jobid, starttime, description)
                            WHERE EXISTS (
                                SELECT 1 FROM {}.job 
                                WHERE job.id = subquery.jobid
                            )
                            ON CONFLICT (id) DO NOTHING 
                        """.format(SCHEMA, SCHEMA), insert_jobinterruption_data, page_size=1000)
        conn.commit()
        # -------------- update jobinterruption endtime -----------------
        for interruption_id, endtime in jobinterruption_endtimes.items():
            # prüft, ob die interruption_id in der Tabelle vorhanden ist
            cur.execute("""
                            SELECT 1
                            FROM {}.jobinterruption
                            WHERE id = %s
                        """.format(SCHEMA), (interruption_id,))
            result = cur.fetchone()

            # Wenn die interruption_id in der Tabelle gefunden wurde, wird Update durchgeführt
            if result is not None:
                cur.execute("""
                                        UPDATE {}.jobinterruption
                                        SET endtime = %s
                                        WHERE id = %s
                                    """.format(SCHEMA), (endtime, interruption_id))
            else:
                print(f'interruption_id {interruption_id, endtime} not found in table, skipping update.')

        # ------------- insert jobstatus in db --------------------
        # erzeugt temporäre tabelle
        temp_table_query = """
                                CREATE TEMP TABLE temp_jobstatus (
                                    jobid VARCHAR(255),
                                    status VARCHAR(255),
                                    timestamp TIMESTAMP
                                ) ON COMMIT DELETE ROWS;
                            """
        cur.execute(temp_table_query)
        #befüllt temporäre Tabelle
        execute_values(cur, 'INSERT INTO temp_jobstatus \
                                    (jobid, status, timestamp) \
                                    VALUES %s',
                                    insert_jobstatus_data,
                                    page_size=1000,
                                    )
        # fügt Daten aus der temporären Tabelle in die jobstatus Tabelle ein, wenn die jobid in der Tabelle job vorhanden ist
        insert_query = """
                                INSERT INTO {}.jobstatus (jobid, status, timestamp)
                                SELECT jobid, status, timestamp 
                                FROM temp_jobstatus 
                                WHERE jobid IN (SELECT id FROM {}.job)
                                ON CONFLICT (jobid, status, timestamp) DO NOTHING;
                            """.format(SCHEMA, SCHEMA)
        cur.execute(insert_query)
        cur.execute("DROP TABLE IF EXISTS temp_jobstatus;")
        # -------------  insert jobsgoals in db --------------------
        execute_values(cur, """
                INSERT INTO {}.jobgoal (id, timestamp, jobid) 
                SELECT id, timestamp,
                (
                    SELECT id
                    FROM {}.job
                    WHERE agvid = subquery.agvid AND starttime <= subquery.timestamp AND subquery.timestamp < endtime
                    ORDER BY starttime
                    LIMIT 1
                ) as jobid
                FROM (
                    VALUES %s
                ) AS subquery(id, timestamp, agvid)
                ON CONFLICT (id, timestamp) DO NOTHING;
                """.format(SCHEMA, SCHEMA), insert_jobgoal_data, page_size=1000)
            
        #--------------  insert agvpositions in db -----------------------
        execute_values(cur, """
            INSERT INTO {}.agvposition
            (pose_x, pose_y, pose_o, agvid, "timestamp", jobid)
            SELECT pose_x, pose_y, pose_o, agvid, "timestamp",
            (
                SELECT id 
                FROM {}.job 
                WHERE agvid = subquery.agvid AND starttime <= subquery.timestamp AND subquery.timestamp < endtime
                ORDER BY starttime
                LIMIT 1
            ) as jobid
            FROM (
                VALUES %s
            ) AS subquery(pose_x, pose_y, pose_o, agvid, "timestamp")
            ON CONFLICT (agvid, "timestamp") DO NOTHING;
            """.format(SCHEMA, SCHEMA), insert_positions_data, page_size=1000,)
        conn.commit()
    except Exception as e:
        print(f"ERROR during db data insertion: {e}")
        if cur:
            cur.close()
        if conn:
            conn.close()
            print('conn closed')