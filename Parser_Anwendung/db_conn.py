import psycopg2
import configparser

# Konfigurationsdatei lesen
config = configparser.ConfigParser()
#Wenn die INI-Datei nicht erkannt wird, muss config.read('db_conn_config.ini') auskommentiert werden und der Pfad der INI-Datei eingetragen werden
#config.read(r'C:\Users\Inna Azarova\Desktop\bachelorAzarova\bachelor\db_conn_config.ini')
config.read('db_conn_config.ini')

# Werte aus der Konfigurationsdatei holen
user = config.get('database', 'user')
password = config.get('database', 'password')
host = config.get('database', 'host')
dbname = config.get('database', 'dbname')
SCHEMA = config.get('database', 'schema')

conn = psycopg2.connect(
    host=f"{host}",
    database=f"{dbname}",
    user=f"{user}",
    password=f"{password}"
)
