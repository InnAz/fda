import re
from datetime import datetime

TIMESTAMP = 23  # 23 Zeichen für die Timestamps im Logfile

def extract_goal_data(row):
    goal_name = row['name']
    pose_x, pose_y = map(float, row['pose'].split(','))  # Aufspaltung 'pose'-Spalte in x und y
    if goal_name:
        return [goal_name, pose_x, pose_y]
    return None

def extract_agvs(line):
    if 'AgvStatus2.FAGV' in line:
        agv_pattern = r'(FAGV\d+)\(([^)]+)\)'
        agv_match = re.search(agv_pattern, line)
        if agv_match:
            agv_id = agv_match.group(1)
            agv_name = agv_match.group(2)
            return (agv_id, agv_name)
    return None

def extract_job_data(line):
    if ("Job-Manager: JobStateChanged:" in line and "state<Scheduled>" in line) or (
            "Job-Manager: NewJob:" in line and "roboter<FAGV" in line):
        job_id_pattern = r'ID<([^>]*)>'
        agv_id_pattern = r'roboter<([^>]*)>'
        source_pattern = r'source<([^>]{1,20})>'
        target_pattern = r'target<([^>]{1,20})>'

        job_id_match = re.search(job_id_pattern, line)
        agv_id_match = re.search(agv_id_pattern, line)
        source_match = re.search(source_pattern, line)
        target_match = re.search(target_pattern, line)

        if job_id_match and agv_id_match and source_match and target_match:
            job_id = job_id_match.group(1)
            agv_id = agv_id_match.group(1)
            source = source_match.group(1)
            target = target_match.group(1)

            if source == agv_id:
                source = "FAGV"

            timestamp_str = line[:TIMESTAMP]
            starttime = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            timestamp2_str = "2023-12-12 00:00:00.000"
            endtime = datetime.strptime(timestamp2_str, '%Y-%m-%d %H:%M:%S.%f')

            return [job_id, agv_id, starttime, endtime, source, target]
    return None

def extract_job_endtimes(line):
    if "Job-Manager: JobStateChanged:" in line and (
            "state<Finished>" in line or "state<Canceled>" in line or "state<Failed>" in line):
        job_id_pattern = r'ID<([^>]*)>'
        job_id_match = re.search(job_id_pattern, line)

        if job_id_match:
            job_id = job_id_match.group(1)
            timestamp_str = line[:TIMESTAMP]
            endttime = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            return job_id, endttime
    return None, None

def extract_jobinterr_endtimes(line):
    if '"description":"the obstacle is gone"' in line:
        interruption_id_pattern = r'affectedJobId":"([^"]*)"'
        interruption_id_match = re.search(interruption_id_pattern, line)

        if interruption_id_match:
            interruption_id = interruption_id_match.group(1)
            timestamp_str = line[:TIMESTAMP]
            endtime = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            if len(interruption_id) >= 20:
                return interruption_id, endtime
    return None,None

def extract_joninterruptio(line):
    if '"description":"stopped by an obstacle"' in line:
        job_id_pattern = r'affectedJobId":"([^_]*)_'
        interruption_id_pat = r'affectedJobId":"([^"]*)"'
        job_id_match = re.search(job_id_pattern, line)
        interruption_id_match = re.search(interruption_id_pat, line)

        if job_id_match and interruption_id_match: 
            job_id = job_id_match.group(1)
            interruption_id = interruption_id_match.group(1)
            timestamp_str = line[:TIMESTAMP]
            starttime = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            description = "stopped by an obstacle"
            if len(job_id) >= 6 and len(job_id) < 8:
                return interruption_id, job_id, starttime, description
    return None,None,None,None

def extract_jobstatus(line):
    if ("Job-Manager: JobStateChanged:" in line or "Job-Manager: NewJob:" in line):
        job_id_pattern = r'ID<([^>]*)>'
        state_pattern = r'state<([^>]*)>'
        job_id_match = re.search(job_id_pattern, line)
        state_match = re.search(state_pattern, line)

        if job_id_match and state_match:
            job_id = job_id_match.group(1)
            status = state_match.group(1)
            timestamp_str = line[:TIMESTAMP]
            timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            return job_id, status, timestamp       
    return None, None, None
    
def extract_jobgoals(line):
    if 'DEBUG' in line and 'Send path from' in line:
        agv_id_pattern = r'Debug ([^:]*):'  
        agv_id_match = re.search(agv_id_pattern, line)
        timestamp_str = line[:TIMESTAMP]
        timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
        agv_id = None
        goals = None

        if agv_id_match:
            agv_id = agv_id_match.group(1)
        goals_start_index = line.find('=>')
        if goals_start_index != -1:
            goals_str = line[goals_start_index + 2:]
            goals = [goal.strip() for goal in goals_str.split(',')]
        if agv_id and goals:
            return [(goal, timestamp, agv_id ) for goal in goals]

    return []

def extract_agvposition(line):
    keyword = "AgvLogStatus.Trace AgvStatus2.FAGV"
    if keyword in line:
        agv_id_pattern = r'\.FAGV(\d{2})\('
        position_pattern = r'x=(-?\d+,\d+);y=(-?\d+,\d+);o=(-?\d+,\d+);'
        agv_id_match = re.search(agv_id_pattern, line)
        position_match = re.search(position_pattern, line)
        if agv_id_match and position_match:
            agvid = f'FAGV{agv_id_match.group(1)}'
            position_x = float(position_match.group(1).replace(',', '.'))
            position_y = float(position_match.group(2).replace(',', '.'))
            position_o = float(position_match.group(3).replace(',', '.'))
            timestamp_str = line[:TIMESTAMP]
            timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S.%f')
            return position_x, position_y, position_o, agvid, timestamp
    return None,None,None,None,None